@extends('layouts.app')

@section('content')
    <div class="login-wrap">
        <a href="index.php"><img  src="{{asset('img/login/QuickUp.jpg')}}" class="logo"></a>
        <div class="login-html">
            <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Iniciar sesión</label>
            <input id="tab-2" type="radio" name="tab" class="for-pwd"><label for="tab-2" class="tab">¿Olvidó su contraseña?</label>
            
            <div class="login-form">
                <form id="loginform" action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="sign-in-htm">
                            <div class="group {{ $errors->has('login') ? 'has-error' : '' }}">
                                    <label for="login" class="col-form-label">E-Mail o Usuario</label>
                                    <input id="login" type="login" class="form-control" name="login" 
                                    value="{{ old('login') }}" required autofocus placeholder="Introduce tu E-Mail o Nombre de Usuario">
                                    @if ($errors->has('login'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('login') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="group">
                                <label for="password" class="col-form-label">Contraseña</label>
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                
                            </div>

                            <div class="group">
                                <label for="Empresa" class="col-form-label" > Empresa</label>
                                <select name="idEmpresa" id="idEmpresa" class="form-control">
                                     
                                        @foreach ($empresas as $item)
                                            <option value="{{$item->Id}}">{{$item->Nombre}}</option>
                                        @endforeach
                                </select>
                            </div>

                        {{-- Input Usuario
                        <div class="group">
                            <label for="user" class="label">Usuario</label>
                            <input type="text" name="user" id="user" placeholder="Usuario" required autofocus class="input">
                        </div>
                        <div class="group">
                            Input password
                            <label for="pass" class="label">Contraseña</label>
                            <input id="pass" name="pass" placeholder="Contraseña" required type="password" class="input" data-type="password">
                        </div>--}}
                        
                        <div class="group">
                            <input type="submit" class="button" value="Acceder">
                        </div>
                        <div class="hr"></div>
                    </div>
                </form>
                <div class="container">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="for-pwd-htm">
                        <div class="group">
                            {{-- <label for="userE" class="label">Correo Electrónico</label>
                            <input id="userE" type="text" class="input"> --}}
                            <label for="email" class="col-form-label">{{ __('CORREO ELECTRÓNICO') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Introduzca su email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div><br>
                        <div class="group">
                            <button type="submit" class="button">
                                {{ __('Enviar Link') }}
                            </button>
                        </div>
                        <div class="hr"></div>
                    </div>
                </form>
                </div>
                
                
            </div>
        </div>
    </div>
@endsection

