@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading" align="center">
                <h3>Nueva campaña de Mercadeo</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-3"></div>
                <div class="col-md-6" align="justify">
                   
                    <form action="{{route('campaña.store')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nombre">Nombre de la campaña:</label>
                                    <input type="text" name="nombre" id="nombre" 
                                        class="form-control" value="{{old('nombre')}}" 
                                        placeholder="Escriba el nombre de la campaña" required
                                    >
                                </div>
                                
                                <div class="form-group">
                                    <label for="descripcion">Descripción de la campaña:</label>
                                    <textarea name="descripcion" id="descripcion" 
                                        cols="30" rows="3" class="form-control"
                                        placeholder="Describe brevemente la campaña">
                                    </textarea>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="fechaInicio">Fecha de Inicio:</label>
                                        <input type="date" name="fechaInicio" id="fechaInicio" 
                                            class="form-control" value="{{old('fechaInicio')}}"
                                        >
                                    </div>
                                    <div class="col-md-6">
                                        <label for="fechaFin">Fecha de Finalización:</label>
                                        <input type="date" name="fechaFin" id="fechaFin" 
                                            class="form-control" value="{{old('fechaFin')}}"
                                        >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="idSucursal">Sucursal:</label>
                                    <select name="idSucursal" id="idSucursal"
                                        class="form-control" >
                                        <option value="null">Seleccione una sucursal:</option>
                                        @foreach ($sucursales as $s)
                                            <option value="{{$s->Id}}">{{$s->SucursalDes}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="form-group" align="center">
                                        <input type="submit" value="Guardar Campaña" class="btn btn-success">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection