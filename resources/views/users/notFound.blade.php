@extends('layouts.app')
@section('content')
{{-- Vista de Usuario NO encontrado en la base de datos --}}
    <div class="container">
        <div class="jumbotron">
            <h1 class="text-center text-danger">Su usuario no ha sido encontrado en la base de datos</h1>
        </div>
    </div>
    
@endsection