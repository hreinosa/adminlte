@extends('admin.layout')

@section('content')

    <table class="table table-bordered table-responsive table-stripped" >
        <thead>
            <tr>
                <th>Nombre</th>
                <th>e-mail</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $item)
                <tr>
                    <td>{{$item->username}}</td>
                    <td>{{$item->email}}</td>
                    <td>
                        <form action="{{Route('users.destroy',array($item->id))}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE" >
                            <a href="{{Route('users.edit',array($item->id))}}" class="btn  btn-warning">Modificar</a>
                            <button class="btn btn-danger" type="Submit">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    
@endsection
