@extends('admin.layout')

@section('content')
   <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading" align="center">
                <h4>Pre-Registros que han confirmado Buró</h4>
            </div>

            <div class="panel-body">
                {{-- <div class="row" align="center">
                    <div class=" col-md-3">
                    <a href="{{route('pre-registro.create')}}" class="btn btn-info">Crear Pre-registro</a> 
                    </div>
                </div> --}}
                <hr>
                <div class="container">
                   <div class="row">
                    <div class="col-md-12 ">
                        <table class="table" id="PreRegistros">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Telefono</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($preRegistros as $item)
                                    <tr>
                                        <td>{{$item->nombre}}</td>
                                        <td>{{$item->telefono}}</td>
                                        <td>{{$item->email}}</td>
                                        <td class="text-danger"><b>{{$item->status}}</b></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                       
                    </div>
                </div> 
                </div>
                
                
            </div>
        </div>
   </div>
@endsection

<script src="{{asset('assets/dist/js/jquery-3.4.1.min.js')}}"></script>
<script>
    $( document ).ready(function() {
        $('#PreRegistros').DataTable({
            "language":{
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });
</script>