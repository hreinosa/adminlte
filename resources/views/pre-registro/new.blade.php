@extends('admin.layout')
@section('content')

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading" align="center">
                <h4>Pre-registro</h4>
            </div>
            <div class="panel-body" align="center">
                <div class="col-md-3"></div>
                <div class=" col-md-6">
                    
                    <form action="{{route('pre-registro.store')}}" role="form" autocomplete="off" method="POST">
                    @csrf
                    <div class="row" >
                        <div class="col-md-6">
                           <div class="form-group ">
                                <label for="nombres">Nombres:</label>
                                <input 
                                    type="text" 
                                    name="nombres" id="nombres" 
                                    class="form-control" 
                                    required value="{{old('nombres')}}" placeholder=""
                                >
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="nombre">Apellidos:</label>
                                <input 
                                    type="text" 
                                    name="apellidos" id="apellidos" 
                                    class="form-control" 
                                    required value="{{old('apellidos')}}" placeholder=""
                                >
                            </div> 
                        </div>
                        
                    </div>
                    <div class="row" >
                        <div class="form-group ">
                            <label for="telefono">Telefono</label>
                            <input 
                                type="text" 
                                name="telefono" id="telefono" 
                                class="form-control" 
                                required value="{{old('telefono')}}"
                                data-mask="0000-0000"
                            >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group ">
                            <label for="email">E-mail:</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{old('email')}}">
                            <input type="checkbox" name="enviar_por_email" value="Sí"> <label for="enviar_por_email">Enviar formulario por correo</label><br>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group ">
                            <label for="vendedor_id">Vendedor Asignado:</label>
                            <select name="vendedor_id" id="vendedor_id" class="form-control">
                                <option value="null">Seleccione un vendedor:</option>
                                @foreach ($vendedores as $vendedor)
                                    <option value="{{$vendedor->id}}">{{$vendedor->firstName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group" align="center">
                            <input type="submit" value="Guardar Pre-registro" class="btn btn-success">
                        </div>
                    </div>
                </form>
                </div>
                
                        
                
                    
                
            </div>
        </div>
    </div>
    
@endsection