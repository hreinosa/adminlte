@extends('admin.layout')

@section('content')
   <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading" align="center">
                <h4>Oportunidades</h4>
            </div>

            <div class="panel-body">
                <div class="row" align="center">
                    <div class=" col-md-3">
                    <a href="{{route('pre-registro.create')}}" class="btn btn-info">Crear Oportunidad</a> 
                    </div>
                </div>
                <hr>
                <div class="container">
                   <div class="row">
                    <div class="col-md-12 ">
                        <table class="table" id="PreRegistros">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>Link</th>
                                    <th class="text-center">Detalles</th>
                                    <th class="text-center">Status</th>
                                    <th>Resultado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($preRegistros as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->nombres . " ". $item->apellidos}}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->id}}">
                                            <i class="fa fa-globe"></i>Link
                                        </button>
                                                
                                        <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header" align="center">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        <label>Link para que el usuario autorice investigación en Buró de Créditos</label>
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body" align="center">
                                                    <a href="{{env('APP_URL', 'http://localhost')}}pre-registro/autorizacion-buro/{{$item->id}}" target="_blank">
                                                        <p>{{env('APP_URL', 'http://localhost')}}pre-registro/autorizacion-buro/{{$item->id}}</p>
                                                    </a>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>

                                    </td>
                                    <td class="text-center">
                                        @if ($item->status == 'Oportunidad')
                                            <p class="text-danger">Usuario no ha completado formulario Buró</p>
                                        @else 
                                            <a href="{{route('pre-registro.detalles',['id' => $item->id])}}" class="btn btn-info"><i class="fa fa-eye"></i> Ver</a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($item->status == 'Pendiente de Evaluación')
                                            <p class="label label-success">{{$item->status}}</p>
                                        @elseif($item->status == 'Oportunidad')
                                            <p class="label label-primary">{{$item->status}}</p>
                                        @elseif($item->status == 'Evaluado')
                                            <p class="label label-warning">{{$item->status}}</p>
                                        @endif
                                    </td>
                                    <td>
                                       @switch($item->resultado_evaluacions_id)
                                           @case(1)
                                                <p class="label label-primary">No asignado</p>
                                               @break
                                           @case(2)
                                               <p class="label label-success">Aprobado</p>
                                               @break
                                            @case(3)
                                                <p class="label label-danger">Rechazado</p>
                                                @break
                                            @case(4)
                                                <p class="label label-info">Condicionado</p>
                                                @break
                                           @default
                                               
                                       @endswitch
                                      
                                       
                                    </td>
                                </tr>
                                    

                                   
                                @endforeach
                            </tbody>
                        </table>
                       
                    </div>
                </div> 
                </div>
                
                
            </div>
        </div>
   </div>
@endsection

<script src="{{asset('assets/dist/js/jquery-3.4.1.min.js')}}"></script>
<script>
    $( document ).ready(function() {
        $('#PreRegistros').DataTable({
            "language":{
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });
</script>


