<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>       
        
    <div class="container" >
        
        <div class="row">
            <div class="col-md-12">
                
                <div class="row">
                    <div class="col-12">
                        <div class="offset-2 text-justify jumbotron">
                            <hr>
                            <div class="jumbotron">
                                <h1 class="display-4 text-info">Hola, {{$preRegistro->nombres}}</h1>
                                <p class="lead">Tus datos han sido procesados exitosamente</p>
                                <hr class="my-4">
                                <p>Estaremos en contacto contigo para hacerte saber el estado de tu solicitud</p>
                                <div >
                                    <img src="{{asset('img/formBuro/Flexiplan.PNG')}}" width="100%">
                                </div>
                                <p class="lead">
                                    <a class="btn btn-primary btn-lg " href="http://flexiplan.com.sv/" role="button" >¡Visita nuestro sitio!</a>
                                </p>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                
                
            </div>
               
        </div>
    </div>
    
</body>
</html>