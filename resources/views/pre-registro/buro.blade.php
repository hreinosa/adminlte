<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap 4.3.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    {{-- Datamask --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <!-- Archivo css -->
    <link rel="stylesheet" href="{{asset('css/formato.css')}}">
    <title>Buro Crediticio</title>
</head>

<body>
    <div class="container">
        <div class="container-fluid col-7">
            <div class="mt-5" id="fondo">
                <hr>
                <div class="">
                    <div class="offset-1">
                        <h2>Autorización de Buró Crediticio</h2>
                        <br>
                        <h6>*FAVOR DIGITAR EN MAYÚSCULA TODA LA INFORMACIÓN</h6>
                        <br>
                        <h6 class="text-danger">*Obligatorio</h6>
                        <br>
                        <h5>FLEXIPLAN, S.A DE C.V</h5>
                        <br>
                    </div>
                    <div class="offset-1">
                        <img src="{{asset('img/formBuro/Flexiplan.PNG')}}" width="100%">
                    </div>
                    <br>
                    <br>
                   
                    <form action="{{route('confirmacion-buro',['id' => $preRegistro->id])}}"  method="POST" >
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div id="nombre" class="offset-1 col-10">
                            <h4>Nombres según DUI
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <label>Escribir acá</label>
                            <br>
                            <br>
                            <input type="text" id="nombres" class="borde" placeholder="Tu respuesta" onblur="nombre(this);" name="nombres" value="{{old('nombres')}}">
                            <br><br>
                            <span class="text-danger" id="spanNombre"></span><br>
                        </div>
                        <br>
                        <div id="apellido" class="offset-1 col-10">
                            <h4>Apellidos según DUI
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <label>Escribir acá</label>
                            <br>
                            <br>
                            <input type="text" id="apellidos" class="borde" placeholder="Tu respuesta" onblur="apellido(this);" name="apellidos" value="{{old('apellidos')}}">
                            <br><br>
                            <span class="text-danger" id="spanApellido"></span><br>
                        </div>
                        <br>
                        <div class="offset-1 col-10" id="medios">
                            <h4>Por qué medio te enteraste de nosotros
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="facebook" name="medio" required value="Facebook">
                                <label class="custom-control-label" for="facebook">FACEBOOK</label>
                            </div>
                            <br>
                            <br>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="instagram" name="medio" required value="Instagram">
                                <label class="custom-control-label" for="instagram">INSTAGRAM</label>
                            </div>
                            <br>
                            <br>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="whatsapp" name="medio" required value="Whatsapp">
                                <label class="custom-control-label" for="whatsapp">WHATSAPP</label>
                            </div>
                            <br>
                            <br>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="otro" name="medio" required value="otro">
                                <label class="custom-control-label" for="otro">Otro:</label>
                            </div>
                            <br>
                            <br>
                            <input type="text" class="borde col-11" id="otroMedio" disabled placeholder="Tu respuesta" onblur="medioContacto(this);" name="otro">
                            <br><br>
                            <span class="text-danger" id="spanOtro"></span><br>
                        </div>
                        <br>
                        <div class="offset-1 col-10" id="redSocial">
                            <h4>Como apareces en tu red social
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <input type="text" id="red" class="borde" placeholder="Tu respuesta" onblur="redSocial(this);" name="social_media_user" value="{{old('social_media_user')}}">
                            <br><br>
                            <span class="text-danger" id="spanRed"></span><br>
                        </div>
                        <br>
                        <div id="departamento" class="offset-1 col-10">
                            <h4>Departamento
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <select id="departamento2" class="borde" placeholder="Tu respuesta" name="departamento" value="{{old('departamento')}}">
                              <option selected value="null">Seleccione un departamento</option>
                              @foreach ($departamentos as $item)
                                <option value="{{$item->id}}">{{$item->EstadoDes}}</option>
                              @endforeach
                            </select>
                            <br><br>
                            <span class="text-danger" id="spanDep"></span><br>
                        </div>
                        <div id="municipio" class="offset-1 col-10">
                                <h4>Municipio
                                    <span class="text-danger">*</span>
                                </h4>
                                <br>
                                <select id="municipio_id" class="borde" placeholder="Tu respuesta" name="municipio_id" value="{{old('municipio_id')}}">
    
                                </select>
                                <br><br>
                                <span class="text-danger" id="spanDep"></span><br>
                            </div>
                        <br>
                        <div id="telefono" class="offset-1 col-10">
                            <h4>Número de Teléfono
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <input type="text" id="telefono2" class="borde" placeholder="Tu respuesta" onblur="telefonoForm(this);" name="telefono" value="{{old('telefono')}}">
                            <br><br>
                            <span class="text-danger" id="spanTel"></span><br>
                        </div>
                        <br>
                        <div id="dui" class="offset-1 col-10">
                            <h4>Número de DUI (sin -)
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <input type="text" id="duiInp" class="borde" placeholder="Tu respuesta" onblur="duiForm(this);" name="dui" value="{{old('dui')}}">
                            <br><br>
                            <span class="text-danger" id="spanDui"></span><br>
                        </div>
                        <br>
                        <div id="nit" class="offset-1 col-10">
                            <h4>Número de NIT (sin -)
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <input type="text" id="nitInp" class="borde" placeholder="Tu respuesta" onblur="nitForm(this);" name="nit" value="{{old('nit')}}">
                            <br><br>
                            <span class="text-danger" id="spanNit"></span><br>
                        </div>
                        <br>
                        <div class="offset-1 col-10">
                            <img src="{{asset('img/formBuro/flex.PNG')}}" width="100%">
                        </div>
                        <br>
                        <div id="acep" class="offset-1 col-10">
                            <h4>Aceptación de Investigación 
                                <span class="text-danger">*</span>
                            </h4>
                            <br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="acepto" name="buro" value="1" value="{{old('buro')}}">
                                <label class="custom-control-label" for="acepto">ACEPTO</label>
                            </div>
                            <span class="text-danger" id="acept"></span><br>
                        </div>
                        <br><br>
                        <div class="offset-1 col-10">
                            <input type="submit" onclick="enviar()" class="btn btn-primary col-2" value="Enviar">
                        </div>
                        <br>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $().ready(function(){
            $("#telefono2").mask('0000-0000');
            $("#duiInp").mask('000000000');
            $("#nitInp").mask('00000000000000');

            
            $("#departamento2").on('change', function(){
                //Obtener el valor del select de departamentos
                var depto = $(this).val();
                //Variable para la ruta de los municipios
                let ruta = "{{ route('municipios', ['id'=>"depto"]) }}";
                ruta = ruta.replace("depto", depto);
                //Peticion AJAX para listar los municipios
                $.ajax({
                    url: ruta,
                    type: "GET",
                    success: function(municipios){
                        $("#municipio_id").empty();
                        municipios.forEach(element => {
                            $("#municipio_id").append('<option value="'+element.id+'">'+element.CiudadesDes+'</option>');
                        });
                    }
                });
            });
        });
    </script>
    <script src="{{asset('js/form.js')}}"></script>
    <br>
    <br>
</body>

</html>