@extends('admin.layout')
@section('content')
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading" align="center">
            <h4>Detalles Oportunidad</h4>
        </div>

        <div class="panel-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('pre-registro.evaluar', ['id'=>$preRegistro->id])}}" method="POST"
                            autocomplete="off">
                            <input type="hidden" name="_method" value="PUT">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" align="center">
                                            Datos Generales
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="nombres">Nombres:</label>
                                                <p>{{$preRegistro->nombres}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="apellidos">Apellidos:</label>
                                                <p>{{$preRegistro->apellidos}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="dui">DUI:</label>
                                                <p>{{$preRegistro->dui}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="dui">NIT:</label>
                                                <p>{{$preRegistro->nit}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">Teléfono:</label>
                                                <p>{{$preRegistro->telefono}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">Departamento:</label>
                                                <p>{{$preRegistro->municipio->departamento->EstadoDes}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">Municipio:</label>
                                                <p>{{$preRegistro->municipio->CiudadesDes}}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" align="center">
                                            Otros Datos
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="email">Email:</label>
                                                <p>{{$preRegistro->email}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="medio">Red Social de Contacto</label>
                                                <p>{{$preRegistro->medio}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="social_media_user">Usuario red social</label>
                                                <p>{{$preRegistro->social_media_user}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="enviar_por_email">¿Recibió información por Correo?</label>
                                                <p>{{$preRegistro->enviar_por_email}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="link">Link</label>
                                                <p>{{env('APP_URL', 'http://localhost')}}pre-registro/autorizacion-buro/{{$preRegistro->id}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <p>{{$preRegistro->status}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="id_vendedor">Vendedor Asignado:</label>
                                                <p>{{$vendedor->firstName." ".$vendedor->lastNames}}</p>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                @if ($preRegistro->status != 'Evaluado')
                                    <div class="col-md-4">
                                        <div class=" panel panel-default">
                                            <div class="panel-heading " align="center" style="background-color:#F39C12;">
                                                <b>Evaluación</b>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="categoria_cliente_id">Categoría cliente:</label>
                                                    <select name="categoria_cliente_id" id="categoria_cliente_id"
                                                        class="form-control">
                                                        @foreach ($categorias as $item)
                                                            <option value="{{$item->id}}">{{$item->Categoria}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="resultado_evaluacions_id">Resultado Evaluación:</label>
                                                    <select name="resultado_evaluacions_id" id="resultado_evaluacions_id"
                                                        class="form-control">
                                                        @foreach ($resultados as $item)
                                                            <option value="{{$item->id}}">{{$item->resultado}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-froup">
                                                    <input type="submit" value="Guardar Evaluación" class="btn btn-success">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($preRegistro->status != 'Pendiente de Evaluación')
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" align="center" style="background-color:#F39C12;">
                                            <b>Resultados de la evaluación</b>
                                        </div>
                                        <div class="panel-body" align="center">
                                            <div class="form-group">
                                                <label for="categoria_cliente_id">Categoría</label>
                                                <p>{{$categ->Categoria}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="resultado_evaluacions_id">Resultado</label>
                                                <p>{{$result->resultado}}</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                @endif
                                

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection