var facebook = document.getElementById('facebook');
var instagram = document.getElementById('instagram');
var whatsapp = document.getElementById('whatsapp');
var otro = document.getElementById('otro');
var texto = document.getElementById('otroMedio');

function updateStatus() {
    if (otro.checked) {
        texto.disabled = false;
    } else {
        texto.disabled = true;
    }
}

facebook.addEventListener('change', updateStatus)
instagram.addEventListener('change', updateStatus)
whatsapp.addEventListener('change', updateStatus)
otro.addEventListener('change', updateStatus)

//Input nombre
function nombre(field) {
    if (field.value == '') {
        document.getElementById("nombre").style.backgroundColor = "#FCE8E6";
        document.getElementById("nombres").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanNombre').innerHTML == '') {
            document.getElementById("spanNombre").append('Esta pregunta es obligatoria');
        }
    }
}


//Input apellido
function apellido(field) {
    if (field.value == '') {
        document.getElementById("apellido").style.backgroundColor = "#FCE8E6";
        document.getElementById("apellidos").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanApellido').innerHTML == '') {
            document.getElementById("spanApellido").append('Esta pregunta es obligatoria');
        }
    }
}

//Input porque medio
function medioContacto(field) {
    if (field.value == '') {
        document.getElementById("otroMedio").style.backgroundColor = "#FCE8E6";
        document.getElementById("medios").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanOtro').innerHTML == '') {
            document.getElementById("spanOtro").append('Esta pregunta es obligatoria');
        }
    }
}

//Input red social
function redSocial(field) {
    if (field.value == '') {
        document.getElementById("red").style.backgroundColor = "#FCE8E6";
        document.getElementById("redSocial").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanRed').innerHTML == '') {
            document.getElementById("spanRed").append('Esta pregunta es obligatoria');
        }
    }
}

//Input departamento
function departamento(field) {
    if (field.value == '') {
        document.getElementById("departamento").style.backgroundColor = "#FCE8E6";
        document.getElementById("departamento2").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanDep').innerHTML == '') {
            document.getElementById("spanDep").append('Esta pregunta es obligatoria');
        }
    }
}

//Input telefono
function telefonoForm(field) {
    if (field.value == '') {
        document.getElementById("telefono").style.backgroundColor = "#FCE8E6";
        document.getElementById("telefono2").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanTel').innerHTML == '') {
            document.getElementById("spanTel").append('Esta pregunta es obligatoria');
        }
    }
}
//Input dui
function duiForm(field) {
    if (field.value == '') {
        document.getElementById("dui").style.backgroundColor = "#FCE8E6";
        document.getElementById("duiInp").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanDui').innerHTML == '') {
            document.getElementById("spanDui").append('Esta pregunta es obligatoria');
        }
    }
}

//Input nit
function nitForm(field) {
    if (field.value == '') {
        document.getElementById("nitInp").style.backgroundColor = "#FCE8E6";
        document.getElementById("nit").style.backgroundColor = "#FCE8E6";
        if (document.getElementById('spanNit').innerHTML == '') {
            document.getElementById("spanNit").append('Esta pregunta es obligatoria');
        }
    }
}

//Funcion enviar sirve para que al momento de dar clic al boton enviar de una alerta de los campos vacios
function enviar() {
    var nombre = document.getElementById("nombres");
    var apellido = document.getElementById("apellidos");
    var medioRed = document.getElementById("otroMedio");
    var redAp = document.getElementById("red");
    var dep = document.getElementById("departamento2");
    var telefono = document.getElementById("telefono2");
    var duiInput = document.getElementById("duiInp");
    var nitInput = document.getElementById("nitInp");
    var acepto = document.getElementById("acepto");

    //Validar input nombre
    if (nombre.value == '') {
        document.getElementById("nombre").style.backgroundColor = "#FCE8E6";
        document.getElementById("nombreA").style.backgroundColor = "#FCE8E6";
        document.getElementById("spanNombre").append('Esta pregunta es obligatoria');
    }

    //Validar input apellido
    if (apellido.value == '') {
        document.getElementById("apellido").style.backgroundColor = "#FCE8E6";
        document.getElementById("apellidos").style.backgroundColor = "#FCE8E6";
        document.getElementById("spanApellido").append('Esta pregunta es obligatoria');
    }

    //Validar input de la redes sociales de contacto
    if (medioRed.value == '') {
        if (!(facebook.checked || instagram.checked || whatsapp.checked)) {
            document.getElementById("otroMedio").style.backgroundColor = "#FCE8E6";
            document.getElementById("medios").style.backgroundColor = "#FCE8E6";
            document.getElementById("spanOtro").append('Esta pregunta es obligatoria');
        } else {
            if (otro.checked) {
                document.getElementById("otroMedio").style.backgroundColor = "#FCE8E6";
                document.getElementById("medios").style.backgroundColor = "#FCE8E6";
                document.getElementById("spanOtro").append('Esta pregunta es obligatoria');
            }
        }
        
    }

    //Validar input red social
    if (redAp.value == '') {
        document.getElementById("red").style.backgroundColor = "#FCE8E6";
        document.getElementById("redSocial").style.backgroundColor = "#FCE8E6";
        document.getElementById("spanRed").append('Esta pregunta es obligatoria');
    }

    //Validar input departamento
    if (dep.value == '' || dep.value == 0) {
        document.getElementById("departamento").style.backgroundColor = "#FCE8E6";
        document.getElementById("departamento2").style.backgroundColor = "#FCE8E6";
        document.getElementById("spanDep").append('Esta pregunta es obligatoria');
    }

    //Validar input telefono
    if (telefono.value == '') {
        document.getElementById("telefono").style.backgroundColor = "#FCE8E6";
        document.getElementById("telefono2").style.backgroundColor = "#FCE8E6";
        document.getElementById("spanTel").append('Esta pregunta es obligatoria');
    }

    //Validar input dui
    if (duiInput.value == '') {
        document.getElementById("dui").style.backgroundColor = "#FCE8E6";
        document.getElementById("duiInp").style.backgroundColor = "#FCE8E6";
        document.getElementById("spanDui").append('Esta pregunta es obligatoria');
    }

    //Validar input nit
    if (nitInput.value == '') {
        document.getElementById("nitInp").style.backgroundColor = "#FCE8E6";
        document.getElementById("nit").style.backgroundColor = "#FCE8E6";
        document.getElementById("spanNit").append('Esta pregunta es obligatoria');
    }

    //Validar radio aceptar
    if (!(acepto.checked)) {
        document.getElementById("acep").style.backgroundColor = "#FCE8E6";
        document.getElementById("acept").append('Esta pregunta es obligatoria');
    }
}