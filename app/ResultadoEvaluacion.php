<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultadoEvaluacion extends Model
{
    protected $fillable =[
        'resultado'
    ];

    public function oportunidad()
    {
        return $this->hasMany('App\PreRegistro','resultado_evaluacions_id');
    }
}
