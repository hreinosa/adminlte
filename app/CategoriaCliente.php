<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaCliente extends Model
{
    protected $fillable = [
        'Categoria'
    ];

    public function preRegistro()
    {
        return $this->hasMany('App\PreRegistro','categoria_cliente_id');
    }


}
