<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreRegistro extends Model
{
    protected $fillable = [
        'nombres', 'apellidos', 'medio','social_media_user','departamento',
        'telefono','dui','nit','buro','enviar_por_email','email', 'categoria_cliente_id',
        'resultado_evaluacions_id','status','vendedor_id'
    ];

    public function categoria()
    {
        return $this->hasOne('App\CategoriaCliente');
    }

    public function resultado()
    {
        return $this->hasOne('App\ResultadoEvaluacion','id');
    }
    
    //Relación Municipio
    public function municipio()
    {
        return $this->belongsTo('App\Municipio');
    }
}
