<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeticionesAjaxController extends Controller
{
    public function departamentos(){
        $departamentos = DB::table("departamentos")->select('departamentos.id', 'EstadoDes')->get();

        return $departamentos;
    }

    public function municipios($id){
        $municipios = DB::table("municipios")->select('municipios.id', 'dep_id', 'CiudadesDes')->where('dep_id', '=', $id)->get();

        return $municipios;
    }
}
