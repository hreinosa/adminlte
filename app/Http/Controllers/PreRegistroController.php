<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Vendedor;
use App\PreRegistro;
use App\CategoriaCliente;
use App\ResultadoEvaluacion;
use App\Mail\BuroForm;
use App\Departamento;
use DB;
use GuzzleHttp\Client;
class PreRegistroController extends Controller
{
    /**--------------Método que retorna la vista de todas las Oportunidades---------------*/

    public function index(){
        /**Obteniendo todos los Pre-registros */
        $preRegistros= PreRegistro::orderBy('id', 'DESC')->get();
      
        return view('pre-registro.index', compact('preRegistros'));
    }
    /**--------------Método que retorna la vista de todas las Oportunidades---------------*/





    /**-----------------Método que retorna la vista para crear nueva Oportunidad-------------------- */
    public function new(){
        /**Peticion a la API para obtener el lista de Vendedores */
        $client = new Client([
            'base_uri'=>'http://localhost:59215/api/',
        ]);
        $response = $client->request('GET', 'crmOportunidades/Vendedores');

        
        $vendedores = json_decode($response->getBody()->getContents());
        return view('pre-registro.new', compact('vendedores'));
    }
    /**-----------------Método que retorna la vista para crear nueva Oportunidad-------------------- */





    /**-----------------Método que almacena los datos de una nueva Oportunidad-------------------- */
    public function store(Request $request){
        $validatedData = $request->validate([
            'nombres'           => 'required|string|max:255',
            'apellidos'         => 'required|string|max:255',
            'telefono'          => 'required|string|max:9',
            'email'             => 'required|string|email|max:200|unique:pre_registros',
            'vendedor_id'       => 'required|integer'
        ]);


        $preRegistro = new PreRegistro();
        $preRegistro->nombres = $request->input('nombres');
        $preRegistro->apellidos = $request->input('apellidos');
        $preRegistro->telefono = $request->input('telefono');
        $preRegistro->email = $request->input('email');
        $preRegistro->enviar_por_email = $request->input('enviar_por_email');
        $preRegistro->vendedor_id = $request->input('vendedor_id');

        if ($preRegistro->enviar_por_email==null) {
            $preRegistro->enviar_por_email= 'No';
            $preRegistro->save();
        }else{
            /**Envía email si ha seleccionado que recibe información por email */
            $preRegistro->save();
            Mail::to($request->input('email'))->send(new BuroForm($preRegistro, $preRegistro->id));
        }
        
        return redirect()->route('pre-registro.index');
    }
    /**-----------------Método que almacena los datos de una nueva Oportunidad-------------------- */






    /**------------------Método que retorna la vista del Formulario de Buró de Créditos------------------ */
    public function autorizacionBuro($id){
        $preRegistro = PreRegistro::find($id);  
        $departamentos = Departamento::all();
        return view('pre-registro.buro', compact('preRegistro', 'departamentos'));
    }
    /**------------------Método que retorna la vista del Formulario de Buró de Créditos------------------ */



    /**-------------Método que almacena la información del formulario de autorización de Buró------------- */
    public function confirmacionBuro(Request $request, $id){
        $validatedData = $request->validate([
            'nombres'           => 'required|string|max:255',
            'apellidos'         => 'required|string|max:255',
            'medio'             => 'required',
            'social_media_user' => 'required|string|max:255',
            'departamento'      => 'required',
            'municipio_id'      => 'required',
            'telefono'          => 'required',
            'dui'               => 'required|string',
            'nit'               => 'required|string',
            'buro'              => 'required'
        ]);

        $preRegistro = PreRegistro::find($id);
        $preRegistro->nombres = $request->input('nombres');
        $preRegistro->apellidos = $request->input('apellidos');
        if ($request->input('medio') == 'otro' ) {
            $preRegistro->medio = $request->input('otro');
        }else {
            $preRegistro->medio = $request->input('medio');
        }
        $preRegistro->social_media_user =$request->input('social_media_user');

        $preRegistro->municipio_id =$request->input('municipio_id');
        $preRegistro->telefono =$request->input('telefono');
        $preRegistro->dui =$request->input('dui');
        $preRegistro->nit =$request->input('nit');
        $preRegistro->buro =$request->input('buro');
        $preRegistro->status ='Pendiente de Evaluación';

        $preRegistro->save();

        return view('pre-registro.evaluacion', compact('preRegistro'));
    }
    /**-------------Método que almacena la información del formulario de autorización de Buró------------- */



    /**--------------------Método que retorna la vista Detalles de la Oportunidad-------------------- */
    public function detalles($id){

        $preRegistro= PreRegistro::find($id);
        $idVendedor = $preRegistro->vendedor_id;

        /**Petición para traer el nombre del vendedor asignado */
        $client = new Client([
            'base_uri'=>'http://localhost:59215/api/',
        ]);
        $response = $client->request('GET', 'crmOportunidades/GetVendedor?idVendedor='.$idVendedor.'');
        $vendedor = json_decode($response->getBody()->getContents());
        /** FIN Petición para traer el nombre del vendedor asignado */
        
        $categorias = CategoriaCliente::all();
        $resultados = ResultadoEvaluacion::all();
        $categ = CategoriaCliente::find($preRegistro->categoria_cliente_id);
        $result = ResultadoEvaluacion::find($preRegistro->resultado_evaluacions_id);


        return view('pre-registro.detalles', compact('preRegistro','vendedor', 'categorias', 'resultados','categ','result'));
    }
    /**--------------------Método que retorna la vista Detalles de la Oportunidad-------------------- */


    /**---------------Método que almacena los cambios de la evaluación de la Oportunidad--------------- */
    public function evaluar(Request $request, $id){
        $preRegistro= PreRegistro::find($id);
        
        $preRegistro->categoria_cliente_id = $request->input('categoria_cliente_id');
        $preRegistro->resultado_evaluacions_id = $request->input('resultado_evaluacions_id');
        $preRegistro->status= 'Evaluado';
        $preRegistro->update();
        return redirect()->route('pre-registro.index');
    }
    /**---------------Método que almacena los cambios de la evaluación de la Oportunidad--------------- */


    

   

 

    
}
