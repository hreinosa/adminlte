<?php

namespace App\Http\Controllers\Auth;
///-----Importaciones AuthenticateUsers--------/////

use Illuminate\Validation\ValidationException;

///-----Importaciones AuthenticateUsers--------/////

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Validator;
use Auth;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   

    //Creando login con email o username

    protected function credentials(Request $request)
    {
        $login = $request->input($this->username());
            // Comprobar si el input coincide con el formato de E-mail
            $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        return [
            $field => $login,
            'password' => $request->input('password')
        ];
    }

    
    public function username()
    {
        return 'login';
    }


    // ------------------------------------------------------------------Metodos de Autenticacion--------------------------------------------------------------------
    public function showLoginForm()
    {
        $client = new Client([
            'base_uri'=>'http://localhost:59215/api/',
        ]);
        $response = $client->request('GET', 'user');
        return view('auth.login',['empresas'=>json_decode($response->getBody()->getContents())]);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    
     /* Metodo modificado de Login*/
    public function login(Request $request)
    {
        $user = $request->input('login');
        $pass = $request->input('password');
        $idEmpresa = $request->input('idEmpresa');

        //Se crea un nuevo cliente
        $c2 = new Client();
         
        //Se establece la conexión y se le envían los parámetros
        $r2 = $c2->request('POST','http://localhost:59215/api/LoginFP/Login?user='.$user.'&pass='.$pass.'&idEmpresa='.$idEmpresa.'');
         
        // Se convierten los datos retornados en un array
        $datosUsuario = json_decode(($r2->getBody()->getContents()),true);

        

        if ($datosUsuario == "Usuario No existe" || $datosUsuario == null) {
            /*
                AQUÍ SE RETORNARÁ UNA VISTA QUE INDIQUE QUE EL USUARIO NO EXISTE EN EL SISTEMA
            */
            return view("users.notFound");

        }elseif ($datosUsuario['id']!=0) {
            /*
                SI EL USUARIO TIENE ID SE BUSCARÁ EN LA BASE DE DATOS DE MYSQL 
            */
            
            if (!User::find($datosUsuario['id'])) {
                /* 
                    SI NO EXISTE CREAREMOS EL REGISTRO EN LA BASE DE DATOS Y DAR INICIO DE SESIÓN
                */
                User::create([
                    'id'=> $datosUsuario['id'],
                    'idperfilenc'=>$datosUsuario['idperfilenc'],
                    'Codigo'=>$datosUsuario['Codigo'],
                    'username'=>$datosUsuario['NombreCompleto'],
                    'password'=> Hash::make($datosUsuario['clave']),
                    'SubUsuario'=> '',
                    'SubIndex'=> '',
                    'activo'=> $datosUsuario['activo'],
                    'Logged'=> $datosUsuario['Logged'],
                    'Sessions'=> $datosUsuario['Sessions'],
                    'Autorizado'=> $datosUsuario['Autorizado'],
                    'KeyVenta'=> $datosUsuario['KeyVenta'],
                    'SobreFac'=> $datosUsuario['SobreFac'],
                    'EsComprador'=> $datosUsuario['EsComprador'],
                    'EsVendedor'=> $datosUsuario['EsVendedor'],
                    'EsCajero'=> $datosUsuario['EsCajero'],
                    'AutorizaCompras'=> $datosUsuario['AutorizaCompras'],
                    'email'=> $datosUsuario['email']
                ]);
                $this->validateLogin($request);

                // If the class is using the ThrottlesLogins trait, we can automatically throttle
                // the login attempts for this application. We'll key this by the username and
                // the IP address of the client making these requests into this application.
                if (method_exists($this, 'hasTooManyLoginAttempts') &&
                    $this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);

                    return $this->sendLockoutResponse($request);
                }

                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($request);
                }

                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
                $this->incrementLoginAttempts($request);

                return $this->sendFailedLoginResponse($request);



            }
            else if(User::find($datosUsuario['id'])){
                /**
                 * PROCEDER AL INICIO DE SESIÓN
                 */
               
                $this->validateLogin($request);

                // If the class is using the ThrottlesLogins trait, we can automatically throttle
                // the login attempts for this application. We'll key this by the username and
                // the IP address of the client making these requests into this application.
                if (method_exists($this, 'hasTooManyLoginAttempts') &&
                    $this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);

                    return $this->sendLockoutResponse($request);
                }

                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($request);
                }

                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
                $this->incrementLoginAttempts($request);

                return $this->sendFailedLoginResponse($request);

            }
        }

    }
      /* Metodo modificado de Login*/

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    // protected function credentials(Request $request)
    // {
    //     return $request->only($this->username(), 'password');
    // }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    // public function username()
    // {
    //     return 'email';
    // }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    // ------------------------------------------------------------------Metodos de Autenticacion--------------------------------------------------------------------

}
