<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\EncabezadoCampañaMercadeo;

class EncabezadoCampañaController extends Controller
{
    public function index(){
        return view('campañas.index');
    }
    
    public function create(){

        /**Peticion a la API para obtener el lista de Sucursales */
        $client = new Client([
            'base_uri'=>'http://localhost:59215/api/',
        ]);
        $response = $client->request('GET', 'mercadeo/GetSucursales');

        
        $sucursales = json_decode($response->getBody()->getContents());

        return view('campañas.encabezadoCreate',compact('sucursales'));
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'nombre'            => 'required|string|max:100',
            'descripcion'       => 'required|string|max:255',
            'fechaInicio'       => 'required|date|after_or_equal:today',
            'fechaFin'          => 'required|date|after:fechaInicio',
            'idSucursal'       => 'required|integer'
        ]);

        $camp = new EncabezadoCampañaMercadeo();
        $camp->nombre = $request->input('nombre');
        $camp->descripcion = $request->input('descripcion');
        $camp->fechaInicio = $request->input('fechaInicio');
        $camp->fechaFin = $request->input('fechaFin');
        $camp->idSucursal = $request->input('idSucursal');
        $camp->save();

        
         return redirect()->action('EncabezadoCampañaController@details', ['id' => $camp->id])
         ->with('success','Campaña agregada exitosamente, por favor agregue las especificaciones de la campaña');
       
    }

    public function details($id){
        $encabezado = EncabezadoCampañaMercadeo::find($id);
        return view('campañas.detallesCreate', compact('encabezado'));
    }
}
