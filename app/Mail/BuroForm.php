<?php

namespace App\Mail;
use App\PreRegistro;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BuroForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $preRegistro;
    public $id;

    public function __construct(PreRegistro $preRegistro, $id)
    {
        $this->$preRegistro = $preRegistro;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('pre-registro.link');
    }
}
