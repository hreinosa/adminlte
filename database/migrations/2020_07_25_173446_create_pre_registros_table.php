<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreRegistrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_registros', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nombres');
            $table->string('apellidos');
            $table->string('medio')->nullable();
            $table->string('social_media_user')->nullable();
            $table->bigInteger('municipio_id')->unsigned()->default(1);
            $table->foreign('municipio_id')->references('id')->on('municipios');
            $table->string('telefono');
            $table->string('dui')->nullable();
            $table->string('nit')->nullable();
            $table->boolean('buro')->default(false);
            $table->string('enviar_por_email')->default('no');
            $table->string('email')->nullable();
            $table->string('status')->default('Oportunidad');

            //Llave foránea Vendedor
            $table->bigInteger('vendedor_id')->unsigned();

            $table->bigInteger('categoria_cliente_id')->unsigned()->default(1);
            $table->foreign('categoria_cliente_id')->references('id')->on('categoria_clientes');

            $table->bigInteger('resultado_evaluacions_id')->unsigned()->default(1);
            $table->foreign('resultado_evaluacions_id')->references('id')->on('resultado_evaluacions');


            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_registros');
    }
}
