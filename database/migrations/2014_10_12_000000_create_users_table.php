<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id');//id
            $table->integer('idperfilenc');//idPerfilEnc
            $table->string('Codigo');//Codigo
            $table->string('username',15)->unique();//NombreCompleto
            $table->string('password');//clave
            $table->string('SubUsuario')->default('');//SubUsuario
            $table->string('SubIndex')->default('');//SubIndex
            $table->boolean('activo')->default(true);//activo
            $table->boolean('Logged')->default(false);//Logged
            $table->integer('Sessions')->default(0);//Sessions
            $table->boolean('Autorizado')->default(false);//Autorizado
            $table->string('KeyVenta')->default('');//KeyVenta
            $table->boolean('SobreFac')->default(false);//SobreFac
            $table->boolean('EsComprador')->default(false);//EsComprador
            $table->boolean('EsVendedor')->default(false);//EsVendedor
            $table->boolean('EsCajero')->default(false);//EsCajero
            $table->boolean('AutorizaCompras')->default(false);//AutorizaCompras
            $table->string('email')->unique();

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
