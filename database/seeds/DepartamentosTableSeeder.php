<?php

use Illuminate\Database\Seeder;
use App\Departamento;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamento = Departamento::create([
            'codigo'=>'01',
            'EstadoDes'=>'Ahuachapan'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'02',
            'EstadoDes'=>'Santa Ana'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'03',
            'EstadoDes'=>'Sonsonate'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'04',
            'EstadoDes'=>'Chalatenango'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'05',
            'EstadoDes'=>'La Libertad'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'06',
            'EstadoDes'=>'San Salvador'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'07',
            'EstadoDes'=>'Cuscatlan'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'08',
            'EstadoDes'=>'La Paz'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'09',
            'EstadoDes'=>'Cabañas'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'10',
            'EstadoDes'=>'San Vicente'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'11',
            'EstadoDes'=>'Usulutan'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'12',
            'EstadoDes'=>'San Miguel'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'13',
            'EstadoDes'=>'Morazan'
        ]);

        $departamento = Departamento::create([
            'codigo'=>'14',
            'EstadoDes'=>'La Union'
        ]);
    }
}
