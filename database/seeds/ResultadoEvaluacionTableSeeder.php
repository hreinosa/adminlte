<?php

use Illuminate\Database\Seeder;
use App\ResultadoEvaluacion;
class ResultadoEvaluacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $resultado = ResultadoEvaluacion::create([
            'resultado'=>'No Asignado'
        ]);
        $resultado = ResultadoEvaluacion::create([
            'resultado'=>'Aprobado'
        ]);
        $resultado = ResultadoEvaluacion::create([
            'resultado'=>'Rechazado'
        ]);
        $resultado = ResultadoEvaluacion::create([
            'resultado'=>'Condicionado'
        ]);
    }
}
