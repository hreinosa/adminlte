<?php

use Illuminate\Database\Seeder;

use App\Municipio;

class MunicipiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0101',
            'CiudadesDes'=>'Ahuachapan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0102',
            'CiudadesDes'=>'Apaneca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0103',
            'CiudadesDes'=>'Atiquizaya'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0104',
            'CiudadesDes'=>'Concepcion de Ataco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0105',
            'CiudadesDes'=>'El Refugio'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0106',
            'CiudadesDes'=>'Guaymango'
        ]);
        
        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0107',
            'CiudadesDes'=>'Jujutla'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0108',
            'CiudadesDes'=>'San Francisco Menendez'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0109',
            'CiudadesDes'=>'San Lorenzo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0110',
            'CiudadesDes'=>'San Pedro Puxtla'
        ]);


        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0111',
            'CiudadesDes'=>'Tacuba'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'1',
            'codigo'=>'0112',
            'CiudadesDes'=>'Turin'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0201',
            'CiudadesDes'=>'Candelaria de La Frontera'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0202',
            'CiudadesDes'=>'Coatepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0203',
            'CiudadesDes'=>'Chalchuapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0204',
            'CiudadesDes'=>'El Congo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0205',
            'CiudadesDes'=>'El Porvenir'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0206',
            'CiudadesDes'=>'Masahuat'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0207',
            'CiudadesDes'=>'Metapan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0208',
            'CiudadesDes'=>'San Antonio Pajonal'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0209',
            'CiudadesDes'=>'San Sebastian Salitrillo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0210',
            'CiudadesDes'=>'Santa Ana'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0211',
            'CiudadesDes'=>'Santa Rosa Guachipilin'
        ]);


        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0212',
            'CiudadesDes'=>'Santiago de La Frontera'
        ]);


        $municipio = Municipio::create([
            'dep_id'=>'2',
            'codigo'=>'0213',
            'CiudadesDes'=>'Texistepeque'
        ]);


        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0301',
            'CiudadesDes'=>'Acajutla'
        ]);


        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0302',
            'CiudadesDes'=>'Armenia'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0303',
            'CiudadesDes'=>'Caluco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0304',
            'CiudadesDes'=>'Cuisnahuat'
        ]);


        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0305',
            'CiudadesDes'=>'Santa Isabel Ishuatan'
        ]);


        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0306',
            'CiudadesDes'=>'Izalco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0307',
            'CiudadesDes'=>'Juayua'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0308',
            'CiudadesDes'=>'Nahuizalco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0309',
            'CiudadesDes'=>'Nahuilingo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0310',
            'CiudadesDes'=>'Salcoatitan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0311',
            'CiudadesDes'=>'San Antonio del Monte'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0312',
            'CiudadesDes'=>'San Julian'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0313',
            'CiudadesDes'=>'Santa Catarina Masahuat'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0314',
            'CiudadesDes'=>'Santo Domingo de Guzman'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0315',
            'CiudadesDes'=>'Sonsonate'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'3',
            'codigo'=>'0316',
            'CiudadesDes'=>'Sonzacate'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0401',
            'CiudadesDes'=>'Agua Caliente'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0402',
            'CiudadesDes'=>'Arcatao'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0403',
            'CiudadesDes'=>'Azacualpa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0404',
            'CiudadesDes'=>'Citala'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0405',
            'CiudadesDes'=>'Comalapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0406',
            'CiudadesDes'=>'Concepcion Quezaltepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0407',
            'CiudadesDes'=>'Chalatenango'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0408',
            'CiudadesDes'=>'Dulce Nombre de Maria'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0409',
            'CiudadesDes'=>'El Carrizal'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0410',
            'CiudadesDes'=>'El Paraiso'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0411',
            'CiudadesDes'=>'La Laguna'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0412',
            'CiudadesDes'=>'La Palma'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0413',
            'CiudadesDes'=>'La Reina'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0414',
            'CiudadesDes'=>'Las Vueltas'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0415',
            'CiudadesDes'=>'Nombre de Jesus'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0416',
            'CiudadesDes'=>'Nueva Concepcion'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0417',
            'CiudadesDes'=>'Nueva Trinidad'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0418',
            'CiudadesDes'=>'Ojos de Agua'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0419',
            'CiudadesDes'=>'Potonico'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0420',
            'CiudadesDes'=>'San Antonio de La Cruz'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0421',
            'CiudadesDes'=>'San Antonio de Los Ranchos'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0422',
            'CiudadesDes'=>'San Fernando'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0423',
            'CiudadesDes'=>'San Francisco Lempa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0424',
            'CiudadesDes'=>'San Francisco Morazan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0425',
            'CiudadesDes'=>'San Ignacio'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0426',
            'CiudadesDes'=>'San Isidro Labrador'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0427',
            'CiudadesDes'=>'San Jose Cancasque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0428',
            'CiudadesDes'=>'San Jose Las Flores'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0429',
            'CiudadesDes'=>'San Luis del Carmen'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0430',
            'CiudadesDes'=>'San Miguel de Mercedes'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0431',
            'CiudadesDes'=>'San Rafael'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0432',
            'CiudadesDes'=>'Santa Rita'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'4',
            'codigo'=>'0433',
            'CiudadesDes'=>'Tejutla'
        ]);
        
        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0501',
            'CiudadesDes'=>'Antiguo Cuscatlán'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0502',
            'CiudadesDes'=>'Ciudad Arce'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0503',
            'CiudadesDes'=>'Colón'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0504',
            'CiudadesDes'=>'Comasagua'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0505',
            'CiudadesDes'=>'Chiltiupan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0506',
            'CiudadesDes'=>'Huizucar'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0507',
            'CiudadesDes'=>'Jayaque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0508',
            'CiudadesDes'=>'Jicalapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0509',
            'CiudadesDes'=>'La Libertad'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0510',
            'CiudadesDes'=>'Nuevo Cuscatlán'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0511',
            'CiudadesDes'=>'Santa Tecla'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0512',
            'CiudadesDes'=>'Quezaltepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0513',
            'CiudadesDes'=>'Sacacoyo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0514',
            'CiudadesDes'=>'San José Villanueva'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0515',
            'CiudadesDes'=>'San Juan Opico'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0516',
            'CiudadesDes'=>'San Matias'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0517',
            'CiudadesDes'=>'San Pablo Tacachico'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0518',
            'CiudadesDes'=>'Tamanique'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0519',
            'CiudadesDes'=>'Talnique'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0520',
            'CiudadesDes'=>'Teotepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0521',
            'CiudadesDes'=>'Tepecoyo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'5',
            'codigo'=>'0522',
            'CiudadesDes'=>'Zaragoza'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0601',
            'CiudadesDes'=>'Aguilares'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0602',
            'CiudadesDes'=>'Apopa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0603',
            'CiudadesDes'=>'Ayutuxtepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0604',
            'CiudadesDes'=>'Cuscatancingo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0605',
            'CiudadesDes'=>'El Paisnal'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0606',
            'CiudadesDes'=>'Guazapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0607',
            'CiudadesDes'=>'Ilopango'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0608',
            'CiudadesDes'=>'Mejicanos'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0609',
            'CiudadesDes'=>'Nejapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0610',
            'CiudadesDes'=>'Panchimalco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0611',
            'CiudadesDes'=>'Rosario de Mora'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0612',
            'CiudadesDes'=>'San Marcos'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0613',
            'CiudadesDes'=>'San Martin'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0614',
            'CiudadesDes'=>'San Salvador'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0615',
            'CiudadesDes'=>'Santiago Texacuangos'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0616',
            'CiudadesDes'=>'Santo Tomas'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0617',
            'CiudadesDes'=>'Soyapango'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0618',
            'CiudadesDes'=>'Tonacatepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'6',
            'codigo'=>'0619',
            'CiudadesDes'=>'Ciudad Delgado'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0701',
            'CiudadesDes'=>'Candelaria'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0702',
            'CiudadesDes'=>'Cojutepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0703',
            'CiudadesDes'=>'El Carmen'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0704',
            'CiudadesDes'=>'El Rosario'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0705',
            'CiudadesDes'=>'Monte San Juan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0706',
            'CiudadesDes'=>'Oratorio de Concepcion'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0707',
            'CiudadesDes'=>'San Bartolome Perulapia'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0708',
            'CiudadesDes'=>'San Cristobal'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0709',
            'CiudadesDes'=>'San Jose Guayabal'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0710',
            'CiudadesDes'=>'San Pedro Perulapan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0711',
            'CiudadesDes'=>'San Rafael Cedros'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0712',
            'CiudadesDes'=>'San Ramon'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0713',
            'CiudadesDes'=>'Santa Cruz Analquito'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0714',
            'CiudadesDes'=>'Santa Cruz Michapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0715',
            'CiudadesDes'=>'Suchitoto'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'7',
            'codigo'=>'0716',
            'CiudadesDes'=>'Tenancingo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0801',
            'CiudadesDes'=>'Cuyultitan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0802',
            'CiudadesDes'=>'El Rosario'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0803',
            'CiudadesDes'=>'Jerusalen'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0804',
            'CiudadesDes'=>'Mercedes La Ceiba'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0805',
            'CiudadesDes'=>'Olocuilta'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0806',
            'CiudadesDes'=>'Paraiso de Osorio'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0807',
            'CiudadesDes'=>'San Antonio Masahuat'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0808',
            'CiudadesDes'=>'San Emigdio'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0809',
            'CiudadesDes'=>'San Francisco Chinameca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0810',
            'CiudadesDes'=>'San Juan Nonualco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0811',
            'CiudadesDes'=>'San Juan Talpa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0812',
            'CiudadesDes'=>'San Juan Tepezontes'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0813',
            'CiudadesDes'=>'San Luis Talpa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0814',
            'CiudadesDes'=>'San Miguel Tepezontes'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0815',
            'CiudadesDes'=>'San Pedro Masahuat'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0816',
            'CiudadesDes'=>'San Pedro Nonualco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0817',
            'CiudadesDes'=>'San Rafael Obrajuelo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0818',
            'CiudadesDes'=>'Santa Maria Ostuma'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0819',
            'CiudadesDes'=>'Santiago Nonualco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0820',
            'CiudadesDes'=>'Tapalhuaca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0821',
            'CiudadesDes'=>'Zacatecoluca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'8',
            'codigo'=>'0822',
            'CiudadesDes'=>'San Luis La Herradura'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0901',
            'CiudadesDes'=>'Cinquera'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0902',
            'CiudadesDes'=>'Guacotecti'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0903',
            'CiudadesDes'=>'Ilobasco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0904',
            'CiudadesDes'=>'Jutiapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0905',
            'CiudadesDes'=>'San Isidro'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0906',
            'CiudadesDes'=>'Sensuntepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0907',
            'CiudadesDes'=>'Tejutepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0908',
            'CiudadesDes'=>'Victoria'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'9',
            'codigo'=>'0909',
            'CiudadesDes'=>'Villa Dolores'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1001',
            'CiudadesDes'=>'Apastepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1002',
            'CiudadesDes'=>'Guadalupe'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1003',
            'CiudadesDes'=>'San Cayetano Istepeque'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1004',
            'CiudadesDes'=>'Santa Clara'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1005',
            'CiudadesDes'=>'Santo Domingo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1006',
            'CiudadesDes'=>'San Esteban Catarina'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1007',
            'CiudadesDes'=>'San Ildefonso'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1008',
            'CiudadesDes'=>'San Lorenzo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1009',
            'CiudadesDes'=>'San Sebastian'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1010',
            'CiudadesDes'=>'San Vicente'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1011',
            'CiudadesDes'=>'Tecoluca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1012',
            'CiudadesDes'=>'Tepetitan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'10',
            'codigo'=>'1013',
            'CiudadesDes'=>'Verapaz'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1101',
            'CiudadesDes'=>'Alegria'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1102',
            'CiudadesDes'=>'Berlin'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1103',
            'CiudadesDes'=>'California'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1104',
            'CiudadesDes'=>'Concepcion Batres'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1105',
            'CiudadesDes'=>'El Triunfo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1106',
            'CiudadesDes'=>'Ereguayquin'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1107',
            'CiudadesDes'=>'Estanzuelas'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1108',
            'CiudadesDes'=>'Jiquilisco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1109',
            'CiudadesDes'=>'Jucuapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1110',
            'CiudadesDes'=>'Jucuaran'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1111',
            'CiudadesDes'=>'Mercedes Umaña'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1112',
            'CiudadesDes'=>'Nueva Granada'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1113',
            'CiudadesDes'=>'Ozatlan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1114',
            'CiudadesDes'=>'Puerto el Triunfo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1115',
            'CiudadesDes'=>'San Agustin'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1116',
            'CiudadesDes'=>'San Buenaventura'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1117',
            'CiudadesDes'=>'San Dionisio'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1118',
            'CiudadesDes'=>'Santa Elena'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1119',
            'CiudadesDes'=>'San Francisco Javier'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1120',
            'CiudadesDes'=>'Santa Maria'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1121',
            'CiudadesDes'=>'Santiago de Maria'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1122',
            'CiudadesDes'=>'Tecapan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'11',
            'codigo'=>'1123',
            'CiudadesDes'=>'Usulutan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1201',
            'CiudadesDes'=>'Carolina'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1202',
            'CiudadesDes'=>'Ciudad Barrios'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1203',
            'CiudadesDes'=>'Comacaran'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1204',
            'CiudadesDes'=>'Chapeltique'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1205',
            'CiudadesDes'=>'Chinameca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1206',
            'CiudadesDes'=>'Chirilagua'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1207',
            'CiudadesDes'=>'El Transito'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1208',
            'CiudadesDes'=>'Lolotique'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1209',
            'CiudadesDes'=>'Moncagua'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1210',
            'CiudadesDes'=>'Nueva Guadalupe'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1211',
            'CiudadesDes'=>'Nuevo Eden de San Juan'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1212',
            'CiudadesDes'=>'Quelepa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1213',
            'CiudadesDes'=>'San Antonio del Mosco'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1214',
            'CiudadesDes'=>'San Gerardo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1215',
            'CiudadesDes'=>'San Jorge'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1216',
            'CiudadesDes'=>'San Luis de La Reina'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1217',
            'CiudadesDes'=>'San Miguel'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1218',
            'CiudadesDes'=>'San Rafael Oriente'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1219',
            'CiudadesDes'=>'Sesori'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'12',
            'codigo'=>'1220',
            'CiudadesDes'=>'Uluazapa'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1301',
            'CiudadesDes'=>'Arambala'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1302',
            'CiudadesDes'=>'Cacaopera'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1303',
            'CiudadesDes'=>'Corinto'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1304',
            'CiudadesDes'=>'Chilanga'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1305',
            'CiudadesDes'=>'Delicias de Concepcion'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1306',
            'CiudadesDes'=>'El Divisadero'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1307',
            'CiudadesDes'=>'El Rosario'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1308',
            'CiudadesDes'=>'Gualococti'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1309',
            'CiudadesDes'=>'Guatajiagua'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1310',
            'CiudadesDes'=>'Joateca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1311',
            'CiudadesDes'=>'Jocoaitique'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1312',
            'CiudadesDes'=>'Jocoro'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1313',
            'CiudadesDes'=>'Lolotiquillo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1314',
            'CiudadesDes'=>'Meanguera'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1315',
            'CiudadesDes'=>'Osicala'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1316',
            'CiudadesDes'=>'Perquin'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1317',
            'CiudadesDes'=>'San Carlos'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1318',
            'CiudadesDes'=>'San Fernando'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1319',
            'CiudadesDes'=>'San Francisco Gotera'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1320',
            'CiudadesDes'=>'San Isidro'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1321',
            'CiudadesDes'=>'San Simon'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1322',
            'CiudadesDes'=>'Sensembra'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1323',
            'CiudadesDes'=>'Sociedad'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1324',
            'CiudadesDes'=>'Torola'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1325',
            'CiudadesDes'=>'Yamabal'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'13',
            'codigo'=>'1326',
            'CiudadesDes'=>'Yoloaiquin'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1401',
            'CiudadesDes'=>'Anamoros'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1402',
            'CiudadesDes'=>'Bolivar'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1403',
            'CiudadesDes'=>'Concepcion de Oriente'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1404',
            'CiudadesDes'=>'Conchagua'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1405',
            'CiudadesDes'=>'El Carmen'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1406',
            'CiudadesDes'=>'El Sauce'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1407',
            'CiudadesDes'=>'Intipuca'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1408',
            'CiudadesDes'=>'La Union'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1409',
            'CiudadesDes'=>'Lislique'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1410',
            'CiudadesDes'=>'Meanguera del Golfo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1411',
            'CiudadesDes'=>'Nueva Esparta'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1412',
            'CiudadesDes'=>'Pasaquina'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1413',
            'CiudadesDes'=>'Poloros'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1414',
            'CiudadesDes'=>'San Alejo'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1415',
            'CiudadesDes'=>'San Jose Las Fuentes'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1416',
            'CiudadesDes'=>'Santa Rosa de Lima'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1417',
            'CiudadesDes'=>'Yayantique'
        ]);

        $municipio = Municipio::create([
            'dep_id'=>'14',
            'codigo'=>'1418',
            'CiudadesDes'=>'Yucuaiquin'
        ]);
    }
}
