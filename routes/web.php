<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@index')->name('home');
Route::get('/', 'Auth\LoginController@showLoginForm')->name('home');

Route::get('admin',function (){
    return view('admin.dashbord');
})->middleware('auth');

Route::resource('/users', 'UserController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function(){
    //Rutas Pre-Registro
    Route::get('pre-registro/index', 'PreRegistroController@index')->name('pre-registro.index');
    Route::get('pre-registro/new', 'PreRegistroController@new')->name('pre-registro.create');
    Route::post('pre-registro/store', 'PreRegistroController@store')->name('pre-registro.store');
    Route::get('pre-registro/{id}', 'PreRegistroController@detalles')->name('pre-registro.detalles');
    Route::put('pre-registro/evaluar/{id}','PreRegistroController@evaluar')->name('pre-registro.evaluar');


    //Rutas para campañas de mercadeo
    Route::get('campaña/new','EncabezadoCampañaController@create')->name('campaña.new');
    Route::get('campaña/index','EncabezadoCampañaController@index')->name('campaña.index');
    Route::post('campaña/store','EncabezadoCampañaController@store')->name('campaña.store');
    Route::get('campaña/{id}/details', 'EncabezadoCampañaController@details')->name('campaña.details');

});
//Ruta que se envía al cliente potencial
Route::get('pre-registro/autorizacion-buro/{id}','PreRegistroController@autorizacionBuro')->name('autorizacion-buro');
//Ruta para actualizar los datos del pre-registro
Route::put('confirmacion-buro/{id}','PreRegistroController@confirmacionBuro')->name('confirmacion-buro');
//Ruta para peticion AJAX que permite el cargado dinamico de los municipios por cada departamento
Route::get('municipios/{id}', 'PeticionesAjaxController@municipios')->name('municipios');






